//
//  ViewController.swift
//  GoogleMarkers
//
//  Created by Indutech on 04/12/17.
//  Copyright © 2017 Jagan. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import GooglePlaces
import GoogleMaps

class ViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate {

    //MARK:-ObjectDecleration
    
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var mapView: GMSMapView!
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 10.0
    var lat = Double()
    var long = Double()
    var selectedPlace: GMSPlace?
    
    
    //MARK:ViewControllerLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addingMapOnViewController()
        self.addMarkers(lattitude: 12.988166, longitude: 80.176506, color: .red,title:"Chennai",subTitle: "India")
        self.addMarkers(lattitude: 19.097403, longitude: 72.874245, color: .blue,title:"Mumbai",subTitle: "India")
        
    }

    //MARK:AddingPolyLines
    
    func addPolyLine(path1Lattitude:Double,path1Longitude:Double,path2Lattitude:Double,path2Longitude:Double,color:UIColor)  {
        let path = GMSMutablePath()
        path.addLatitude(path1Lattitude, longitude: path1Longitude)
        path.addLatitude(path2Lattitude, longitude: path2Longitude)
        let polyline = GMSPolyline(path: path)
        polyline.spans = [GMSStyleSpan(color:color)]
        polyline.strokeWidth = 10.0
        polyline.geodesic = true
        polyline.map = mapView
        
    }
    
    //MARK:-AddingMarkers
    
    func addMarkers(lattitude:Double,longitude:Double,color:UIColor,title:String,subTitle:String)  {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lattitude, longitude: longitude)
        marker.title = title
        marker.snippet = subTitle
        marker.icon = GMSMarker.markerImage(with:color)
        marker.map = mapView
        
    }
    
    //MARK:AddingMapOnViewController
    
    func addingMapOnViewController() {
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        placesClient = GMSPlacesClient.shared()
        
        
        let defaultLocation = CLLocation(latitude: -33.869405, longitude: 151.199)
        let camera = GMSCameraPosition.camera(withLatitude: defaultLocation.coordinate.latitude,longitude: defaultLocation.coordinate.longitude,
                                              zoom: zoomLevel)
        mapView = GMSMapView.map(withFrame: view.bounds, camera: camera)
        mapView.settings.myLocationButton = true
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(mapView)
        
        let button = UIButton(frame: CGRect(x:(view.frame.size.width)/2.5, y: 20, width: 150, height: 50))
        button.backgroundColor = UIColor.blue.withAlphaComponent(0.5)
        button.setTitle("Get Directions", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        self.mapView.addSubview(button)
        mapView.isHidden = true
        
    }
    
    //MARK:DrawingPolyDirections
    
    func buttonAction(sender: UIButton!) {
        
        self.addPolyLine(path1Lattitude:self.lat,path1Longitude:self.long,path2Lattitude:12.988166,path2Longitude:80.176506,color:.blue)
        
        self.addPolyLine(path1Lattitude:self.lat,path1Longitude:self.long,path2Lattitude:19.097403,path2Longitude:72.874245,color:.red)
    }
    
    
    //MARK:-CLLocationManagerdelegates
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: zoomLevel)
        
        self.lat =  location.coordinate.latitude
        self.long =  location.coordinate.longitude
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        marker.title = "Bangalore"
        marker.snippet = "Karnataka"
        marker.icon = GMSMarker.markerImage(with:.green)
        marker.map = mapView
        
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
            
            
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    

}

